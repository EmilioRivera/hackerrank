const fs = require('fs');
const _ = require('lodash');
const net = require('net');
const userName = 'Emilio Rivera'
const URL = `polyhang.ddns.net`;

const dic = fs.readFileSync('dictionnaire.txt', 'latin1');
const words = dic.split('\n')
// const wordsOfLength = buildWordsForLength(words);
const allLetters = _.uniq(words.join(''));
const specialChars = _.filter(allLetters, (uniques) => /[^a-z]/gi.test(uniques));

const wordsOfLength = _.groupBy(words, 'length');
const charStats = buildCharStats(words);
const perLengthCharStats = _.mapValues(wordsOfLength, (words, length) => {
    return buildCharStats(words)
});
console.log(charStats)


function buildCharStats(words) {
    // Create a character apparition in language, i.e, how many times a letter is used in this language
    // return _.sortBy(_.toPairs(_.countBy(words.join(''))), (o) => o[1]).reverse();
    
    // Create a character apparition in words frequency dictionnary,
    // i.e how many words each character appears in
    let lettInWords = allLetters.map((lett, lett_i) => {
        return [lett, _.sumBy(words, (v) => v.indexOf(lett) != -1)]
    });
    const spCount = _.sumBy(lettInWords.filter((v,i) => /[^a-z]/gi.test(v[0])), (o) => o[1])
    _.remove(lettInWords, (v,i,a) => /[^a-z]/gi.test(v[0]))
    lettInWords.push([undefined, spCount])
    return _.sortBy(lettInWords, (o) => o[1]).reverse();
}

// for (const key in wordsOfLength) {
//     if (wordsOfLength.hasOwnProperty(key)) {
//         const element = wordsOfLength[key];
//         console.log(`Length is ${element.length}`)
//     }
// }

const connectServer = async () => {
    return await net.createConnection({
        host: URL,
        port: 4343,
    });
}

function sendName(socket) {
    return new Promise((res, rej) => {
        socket.once('data', (data) => {
            console.log('Data is', data.toString())
            // Answer name
            socket.once('data', (responseToLogin) => {
                // console.log(`Got ${responseToLogin}`)
                res(responseToLogin);
            });
            console.log(`Writing ${userName} to socket`)
            socket.write(Buffer.from(userName, 'utf16le'))
        });
    });
}
function parseChallenge(info, isFirstInfo) {
    console.log(`We try to parse\n${info}`);
    // console.log(info.isEncoding(ENCODING));
    const s = info.split('#')
    const alph = s[isFirstInfo ? 0 : 2].split('\n').slice(1, -1)
    const secret = s[isFirstInfo ? 1 : 3].substr(17)
    console.log(`Length of ${secret} is`, secret.length)
    const ix_to_c = alph.reduce((p, value, ix) => {
        (p || {})[ix] = value[0];
        return p;
    }, {});
    const c_to_ix = _.invert(ix_to_c)
    // console.log(ix_to_c)
    return [ix_to_c, c_to_ix, secret];
}


const ENCODING = 'utf16le';
const unk_tok = Buffer.from([253, 255]).toString('utf16le');
const rpl = new RegExp(`_|${unk_tok}`, 'g');
const specialCharRepl = `[${specialChars.join('')}]`;
function buildRegex(secret, ctix) {
    const char = `[${_.without(_.keys(ctix), unk_tok).join('')}]`
    // const _r = secret.replace(rpl, char);
    const _r = secret.replace(/_/g, char).replace(rpl, specialCharRepl);
    // console.log(_r)
    const r = new RegExp(`^${_r}$`)
    return r;
}

function getCandidates(words, regex) {
    return words.filter((w) => regex.test(w));
}

function unknown_length(secret) {
    return (secret.match(/_/g)||[]).length
}

function stat_sample(sortedStats, c_to_ix, forceConsonnant) {
    for (let [c, count] of sortedStats) {
        if (forceConsonnant && isVowel(c)) {
            continue
        }
        if (c_to_ix.hasOwnProperty(c)) {
            return c_to_ix[c];
        }
    }
    // If failure to get something worthwhile
    return _.sample(c_to_ix);
}

function isVowel(char) {
    return new RegExp(`[aeiouy${unk_tok}']`, 'g').test(char)
    // return /[aeiouy']/g.test(char);
}

const guessNextLetterIndex = (ix_to_c, c_to_ix, secret) => {
    // First try is always special char
    // if (c_to_ix.hasOwnProperty(unk_tok)) {
    //     console.log(`Special char detected, trying this one first`)
    //     return c_to_ix[unk_tok]
    // }

    let stats_for_length = _.cloneDeep(perLengthCharStats[secret.length]); // || charStats

    // Heuristic for words
    const u = unknown_length(secret);
    const f = u - secret.length;
    if (secret.indexOf('q_e') !== -1 && _.has(c_to_ix, 'u')) {
        return c_to_ix['u'];
    }
    // const onlyVowelsFound = u !== secret.length ? _.every(secret, (c) => {
    //     return (c === unk_tok) || /[aeiouy_']/g.test(c);
    // }) : false;
    const forceConsonnant = false; //onlyVowelsFound && (_.uniq(secret).length > 2) && (secret.length > 3);

    // TODO: Check which condition should trigger regex search
    if (true || u < 0.2 * secret.length || f > 2) {
        const reg = buildRegex(secret, c_to_ix);
        const candidates = getCandidates(wordsOfLength[secret.length], reg)
        // console.log(`Found ${candidates.length} candidates for regex ${reg}`)
        console.log(`Found ${candidates.length} candidates for regex`)
        if (candidates.length === 1) {
            const search_i = secret.indexOf('_');
            const search_token = candidates[0][search_i];
            return c_to_ix[search_token];
        }
        // if (candidates.length < 100000) {
            const uniques = _.uniq(candidates.join(''))
            const shared = _.intersection(uniques, _.keys(c_to_ix));
            console.log(`Shared values are ${shared}`);
            const sh = _.pick(c_to_ix, shared);
            // return c_to_ix[_.sample(shared)];
            stats_for_length = buildCharStats(candidates)
            return stat_sample(stats_for_length, sh, forceConsonnant);
        // }
    }
    console.log(`Using dict ${_.size(ix_to_c)} to guess string ${secret}`)
    return stat_sample(stats_for_length, c_to_ix, forceConsonnant)
    // return _.sample(_.keys(ix_to_c));
}

connectServer().then(socket => {
    socket.setEncoding(ENCODING);
    socket.setTimeout(10000);
    // socket.setNoDelay(true)
    sendName(socket).then( async (value) => {
        // console.log(value.toString())
        // console.log(iv.convert(value).toString('latin1'))
        // fs.writeFileSync('challenge1.txt', info)
        // console.log(`Dict is ${dict}, secret is ${secret}`)
        const sendLetterGuess = (socket, g) => {
            return new Promise((resolve) => {
                const payload = Buffer.from(g, 'utf16le');
                socket.write(payload);
                socket.once('data', (res) => {
                    resolve(res);
                });
            });
        }
        let challengeInfo = value;
        let tot_found = 0;
        const foundWords = [];

        socket.on('error', (err) => {
            console.error(err);
            socket.end();
        });

        socket.on('end', () => {
            console.log(`Nombre total de mots trouvés: ${tot_found}`);
            fs.appendFileSync('found.txt', foundWords.join('\n'))
        })
        
        while (true) {
            let [ix_to_char, char_to_ix, secret] = parseChallenge(challengeInfo, tot_found === 0);
            for (;;) {
                let guess = guessNextLetterIndex(ix_to_char, char_to_ix, secret);
                const letter = ix_to_char[guess];
                // TODO: Change
                if (letter === undefined) {
                    guess = char_to_ix[unk_tok];
                }
                const reponse = await sendLetterGuess(socket, guess.toString())
                ix_to_char = _.omit(ix_to_char, guess);
                char_to_ix = _.invert(ix_to_char)

                if (reponse.startsWith(`C'est réussi`)) {
                    console.log(`Success1!!: ${reponse}, tot ${tot_found+1}`)
                    foundWords.push(secret.replace(/_/g, letter))
                    challengeInfo = reponse;
                    tot_found++;
                    break;
                } else {
                    const _r = reponse.split('#')
                    
                    const nSecret = _r[0].substr(17);
                    if (secret === nSecret) {
                        console.log(`OUCH letter ${letter}`);
                    } else {
                        console.log(`YESS letter ${letter}`);
                    }
                    secret = nSecret;
                    console.log(`Response to ${guess} (${letter}) is `, secret);
                }
            }
        }
    })
});
